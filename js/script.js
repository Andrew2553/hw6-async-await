// Асинхронність у JavaScript - дозволяє виконувати операції без чекання завершення попередньої операції.


document.addEventListener("DOMContentLoaded", () => {
    const findIpButton = document.getElementById("findIpButton");
    const resultDiv = document.getElementById("result");

    findIpButton.addEventListener("click", async () => {
        try {
           
            const ipResponse = await fetch("https://api.ipify.org/?format=json");
            const ipData = await ipResponse.json();
            const clientIp = ipData.ip;

            
            const locationResponse = await fetch(`http://ip-api.com/json/${clientIp}`);
            const locationData = await locationResponse.json();

           
            resultDiv.innerHTML = `
                <p>IP адреса клієнта: ${clientIp}</p>
                <p>AS: ${locationData.as}</p>
                <p>Місто: ${locationData.city}</p>
                <p>Країна: ${locationData.country}</p>
                <p>Код Країни: ${locationData.countryCode}</p>
                <p>Провайдер: ${locationData.isp}</p>
                <p>Широта: ${locationData.lat}</p>
                <p>Довгота: ${locationData.lon}</p>
                <p>Query: ${locationData.query}</p>
                <p>Код регіону: ${locationData.region}</p>
                <p>Регіон: ${locationData.regionName}</p>
                <p>Статус: ${locationData.status}</p>
                <p>Часовий пояс: ${locationData.timezone}</p>            
                <p>Почтовий індекс: ${locationData.zip}</p>
            `;
        } catch (error) {
            console.error("Помилка при отриманні інформації:", error);
        }
    });
});